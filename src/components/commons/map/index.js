import React, {useState, useEffect, useRef} from 'react';
import styled from 'styled-components/native';
import {Dimensions, StyleSheet} from 'react-native';
import {Pointer} from '@commons/MapMarkers';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

const {width, height} = Dimensions.get('window');

const MapContainer = styled.View`
  flex: 1;
  width: 100%;
  flex-direction: column;
  align-items: center;
  background-color: transparent;
`;

const MapComponent = () => {
  const ASPECT_RATIO = width / height;
  const [getLatitude, setgetLatitude] = useState();
  const [getLongitude, setgetLongitude] = useState();
  const [getLatitudeDelta] = useState(0.0922);
  const [getLongitudeDelta] = useState(getLatitudeDelta * ASPECT_RATIO);
  const map = useRef(null);

  useEffect(() => {
    let isUnMounted = false;
    _findCoordinates();

    return () => {
      isUnMounted = true;
    };
  }, []);

  const _findCoordinates = () => {
    Geolocation.getCurrentPosition(
      position => {
        setgetLatitude(position.coords.latitude);
        setgetLongitude(position.coords.longitude);
        // console.log(position.coords.latitude);
      },
      error => console.warn(error.message),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
      },
    );
  };

  return (
    <MapContainer>
      {getLatitude && getLongitude ? (
        <MapView
          ref={map}
          provider={PROVIDER_GOOGLE}
          style={{
            ...StyleSheet.absoluteFillObject,
          }}
          // customMapStyle={themeContext.MAPSTYLE}
          // initialRegion
          region={{
            latitude: getLatitude ? getLatitude : getLatitude,
            longitude: getLongitude ? getLongitude : getLongitude,
            latitudeDelta: getLatitudeDelta,
            longitudeDelta: getLongitudeDelta,
          }}>
          <Marker
            coordinate={{
              latitude: getLatitude,
              longitude: getLongitude,
              latitudeDelta: getLatitudeDelta,
              longitudeDelta: getLongitudeDelta,
            }}>
            <Pointer background="#333" />
          </Marker>
        </MapView>
      ) : null}
    </MapContainer>
  );
};

export default MapComponent;
