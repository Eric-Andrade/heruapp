import React from 'react';
import {TouchableOpacity} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import MainScreen from '@screens/Main';
import UsersScreen from '@screens/Users';
import UserScreen from '@screens/Users/User';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {FontAwesome} from '@icons';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerTransparent: true,
        headerShown: true,
      }}>
      <Drawer.Screen name="Bienvenido" component={MainScreen} />
      <Drawer.Screen
        name="Users"
        component={UsersScreen}
        options={({navigation, route}) => ({
          title: 'Todos los usuarios',
          headerTransparent: true,
          cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
        })}
      />
    </Drawer.Navigator>
  );
};

const Navigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={DrawerNavigation}
          options={({navigation, route}) => ({
            title: 'Welcome',
            headerShown: !true,
            headerTransparent: true,
          })}
        />
        <Stack.Screen
          name="User"
          component={UserScreen}
          options={({navigation, route}) => ({
            title: '',
            headerShown: true,
            headerLeft: () => (
              <TouchableOpacity
                style={{
                  marginLeft: 10,
                  height: 40,
                  width: 40,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={navigation.goBack}>
                <FontAwesome name="angle-left" size={25} color="#333" />
              </TouchableOpacity>
            ),
            headerTransparent: !true,
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
