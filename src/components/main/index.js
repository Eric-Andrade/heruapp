import React from 'react';
import styled from 'styled-components/native';
import {ETAClock, ETAButtonFilled} from '@etaui';
import {
  View,
  StyleSheet,
  Image,
  Text,
  ImageBackground,
  ScrollView,
} from 'react-native';
import MapComponent from '@commons/map';
import {useNavigation} from '@react-navigation/native';

const logoSize = 70;

const Root = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  background-color: transparent;
`;

const MainComponent = () => {
  const navigation = useNavigation();
  const _navigateGetUsers = () => {
    navigation.navigate('Users');
  }

  return (
    <Root>
      <View style={styles.bannerContainer}>
        <ImageBackground
          style={styles.banner}
          source={require('@assets/doodle.webp')}>
          <View style={styles.logoContainer}>
            <Image
              style={styles.logo}
              source={require('@assets/app-icon.webp')}
            />
          </View>
          <View style={styles.welcomeTextContainer}>
            <Text style={styles.welcomeText}>Bienvenido</Text>
            <ETAClock />
          </View>
        </ImageBackground>
      </View>
      <ScrollView 
        styles={styles.scrollContainer}>
        <View style={styles.paragraghContainer}>
          <Text style={styles.paragraphText}>
            Dolorem voluptatem et ab adipisci aut aspernatur iusto corporis.
            Dolorem sunt et. Nesciunt distinctio cum assumenda. Quos autem nobis
            ut ea autem quas sit sit dicta.
          </Text>
        </View>
        <View style={styles.buttonContainer}>
          <ETAButtonFilled
            title="Todos los usuarios"
            onPress={() => _navigateGetUsers()}
            colorButton="#2D2C3C"
            padding={15}
            width={155}
            borderRadius={3}
            borderWidth={0.5}
          />
        </View>
        <View style={styles.mapContainer}>
          <MapComponent />
        </View>
        <View style={styles.newsContainer}>
          <View style={styles.newsItem}>
            <Text style={styles.newsItemText}>
              Quia fugiat doloremque earum commodi nemo quasi consequatur. Dicta
              exercitationem minus enim et atque voluptatem consequatur aliquam.
              Odit beatae rerum sint aut eum non. Sunt asperiores ad velit velit
              sequi aut nam et voluptas.
            </Text>
          </View>
          <View style={styles.newsItem}>
            <Text style={styles.newsItemText}>
              Occaecati est animi laborum non neque est. Fugit nostrum ab
              voluptas qui. Ad amet est ratione officiis dolorem dignissimos
              perspiciatis voluptatem optio.
            </Text>
          </View>
          <View style={styles.newsItem}>
            <Text style={styles.newsItemText}>
              Occaecati est animi laborum non neque est. Fugit nostrum ab
              voluptas qui. Ad amet est ratione officiis dolorem dignissimos
              perspiciatis voluptatem optio.
            </Text>
          </View>
          <View style={styles.newsItem}>
            <Text style={styles.newsItemText}>
              Iste est tenetur ipsam. At ut officiis molestias dolore quam cum
              sed. Labore alias dolor sit dolorem sequi eos ullam fuga quaerat.
              Reiciendis ea earum amet sed. Necessitatibus dolor quia quibusdam
              earum itaque enim. Maiores aperiam saepe perspiciatis temporibus
              minus et odio. Rerum maiores neque incidunt. Mollitia praesentium
              nam. Omnis sed ut unde molestiae odio minus nemo. Asperiores
              maiores ipsum quibusdam enim et occaecati. Facere rerum minima
              placeat maxime accusamus assumenda incidunt sint dolore. Ad facere
              totam qui reiciendis. Hic est architecto fuga expedita ut facere
              cumque. Doloremque at quo et asperiores. Velit quibusdam maxime
              vel nobis iure. Autem deleniti minus odio quia accusantium
              repellat enim.
            </Text>
          </View>
        </View>
      </ScrollView>
    </Root>
  );
};

const styles = StyleSheet.create({
  bannerContainer: {
    height: 180,
    minWidth: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1000,
  },
  banner: {
    flex: 1,
    minWidth: '100%',
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoContainer: {
    width: logoSize + 20,
    height: logoSize + 20,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  logo: {
    height: logoSize,
    width: logoSize,
  },
  welcomeTextContainer: {
    marginVertical: 7,
    padding: 5,
    borderRadius: 7,
    borderWidth: 0.3,
    borderColor: '#333',
    backgroundColor: '#fff',
  },
  welcomeText: {
    fontSize: 16,
    textAlign: 'center',
    color: '#2D2C3C',
    fontFamily: 'Arial',
    fontWeight: 'bold',
  },
  scrollContainer: {
    flex: 1,
    alignItems: 'center',
    marginHorizontal: 50,
    backgroundColor: 'transparent',
  },
  paragraghContainer: {
    marginHorizontal: 15,
    marginTop: 7,
    borderRadius: 7,
    borderWidth: 0.3,
    borderColor: '#333',
    padding: 10,
    backgroundColor: '#65D9E4',
  },
  paragraphText: {
    fontSize: 14,
    fontFamily: 'Arial',
    color: '#fff',
  },
  buttonContainer: {
    marginHorizontal: 15,
    marginVertical: 5,
    alignSelf: 'flex-end',
    justifyContent: 'center',
  },
  mapContainer: {
    height: 160,
    width: '96%',
    alignSelf: 'center',
    padding: 7,
    marginHorizontal: 10,
    borderRadius: 10,
    borderWidth: 0.2,
    borderColor: '#333',
  },
  newsContainer: {
    marginTop: 10,
    marginHorizontal: 10,
    padding: 10,
    backgroundColor: '#fff',
  },
  newsItem: {
    marginVertical: 4,
  },
  newsItemText: {
    fontSize: 14,
    fontFamily: 'Arial',
    textAlign: 'justify',
    color: '#333',
  },
});

// eslint-ignore
export default MainComponent
