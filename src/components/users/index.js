import React, {useEffect} from 'react';
import {View, Text, FlatList, StyleSheet, Platform} from 'react-native';
import styled from 'styled-components/native';
import {ETASimpleText, ETALoader} from '@etaui';
import {connect} from 'react-redux';
import {GET_DATA_REQUEST} from '@redux/users/actions';
import UsersItemComponent from './item';

const Root = styled.View`
  flex: 1;
  justify-content: flex-start;
  align-items: center;
  background-color: transparent;
`;
const EmptyListContainer = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 100px;
  background-color: transparent;
`;

const mapStateToProps = (state, props) => {
  const {data} = state.users;
  return {data};
};

const mapDispatchProps = (dispatch, props) => ({
  getDataRequest: () => {
    dispatch({
      type: GET_DATA_REQUEST,
      payload: {},
    });
  },
});

const UsersComponent = ({getDataRequest, data}) => {
  const ref = React.useRef(null);

  useEffect(() => {
    getDataRequest();
  }, [getDataRequest]);

  return (
    <Root>
      <View style={styles.paragraghContainer}>
        <Text style={styles.paragraphText}>
          Para ver más información acerca de un usuario solo debes elegir uno y
          dar click en el nombre.
        </Text>
      </View>
      {data.length > 0 ? (
        <FlatList
          ref={ref}
          contentContainerStyle={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-start',
          }}
          data={data}
          keyExtractor={(item, index) => index.toString()}
          horizontal={!true}
          initialNumToRender={15}
          showsVerticalScrollIndicator={false}
          updateCellsBatchingPeriod={3000}
          ListEmptyComponent={() => (
            <EmptyListContainer>
              <ETASimpleText
                size={14}
                weight={Platform.OS === 'ios' ? '400' : '300'}
                color="#fff"
                align="left">
                No hay usuarios aún
              </ETASimpleText>
            </EmptyListContainer>
          )}
          renderItem={({item, i}) => {
            return <UsersItemComponent key={i} {...item} />;
          }}
        />
      ) : (
        <ETALoader color="#65D9E4" size={9} />
      )}
    </Root>
  );
};

const styles = StyleSheet.create({
  paragraghContainer: {
    marginHorizontal: 15,
    marginVertical: 10,
    borderRadius: 7,
    padding: 10,
    backgroundColor: '#65D9E4',
  },
  paragraphText: {
    fontSize: 14,
    fontFamily: 'Arial',
    color: '#fff',
  },
});

const UsersComponentConnect = connect(
  mapStateToProps,
  mapDispatchProps,
)(UsersComponent);

export default UsersComponentConnect;
