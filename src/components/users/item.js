import React from 'react';
import {TouchableOpacity, StyleSheet, Platform} from 'react-native';
import styled from 'styled-components/native';
import {ETASimpleText} from '@etaui';
import {useNavigation} from '@react-navigation/native';
import {FontAwesome} from '@icons';

const Root = styled.View`
  height: 40px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-color: #2d2c3c;
  border-bottom-width: 0.4px;
`;

const UsersItemComponent = params => {
  const navigation = useNavigation();

  const _navigateGetUser = propitem => {
    navigation.navigate('User', {
      params: {
        item: propitem,
      },
    });
  };

  return (
    <Root>
      <TouchableOpacity
        style={styles.detailsButtonContainer}
        onPress={() => _navigateGetUser(params)}>
        <ETASimpleText
          size={14}
          weight={Platform.OS === 'ios' ? '400' : '300'}
          color="#2d2c3c"
          align="right">
          {params.name}
        </ETASimpleText>
        <FontAwesome name="angle-right" size={25} color="#2d2c3c" />
      </TouchableOpacity>
    </Root>
  );
};

const styles = StyleSheet.create({
  detailsButtonContainer: {
    flexDirection: 'row',
    height: 20,
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    // marginHorizontal: 10,
    zIndex: 1000,
  },
});
export default UsersItemComponent;
