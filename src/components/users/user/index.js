import React, {useState, useRef} from 'react';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {ETASimpleText} from '@etaui';
import {Pointer} from '@commons/MapMarkers';
import {
  View,
  Platform,
  StyleSheet,
  Image,
  Linking,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {Fontisto} from '@icons';

const logoSize = 100;
const avatarRadius = logoSize / 2;

const UserComponent = () => {
  const route = useRoute();
  const {item} = route.params.params;
  const [getLatitudeDelta] = useState(0.015);
  const [getLongitudeDelta] = useState(0.0121);
  const map = useRef(null);

  const openLink = url => {
    Linking.openURL(url).catch(err => console.error('An error occurred', err));
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image style={styles.avatar} source={require('@assets/profile.webp')} />
        <View style={styles.usernameContainer}>
          <ETASimpleText
            size={14}
            weight={Platform.OS === 'ios' ? '400' : '300'}
            color="#fff"
            align="center">
            {item.username}
          </ETASimpleText>
        </View>
      </View>
      <View style={styles.dataContainer}>
        <ETASimpleText
          size={14}
          weight={Platform.OS === 'ios' ? '400' : '300'}
          color="#2d2c3c"
          align="center">
          {item.name}
        </ETASimpleText>
        <ETASimpleText
          size={14}
          weight={Platform.OS === 'ios' ? '400' : '300'}
          color="#2d2c3c"
          align="center">
          {item.email}
        </ETASimpleText>
        <ETASimpleText
          size={14}
          weight={Platform.OS === 'ios' ? '400' : '300'}
          color="#2d2c3c"
          align="center">
          {item.phone}
        </ETASimpleText>
      </View>
      <View style={styles.actionsContainer}>
        <TouchableOpacity
          onPress={() => openLink(`tel:${item.phone}`)}
          style={styles.actionContainer}>
          <Fontisto name="phone" size={18} color="#fff" />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => openLink(`mailto:${item.email}`)}
          style={styles.actionContainer}>
          <Fontisto name="email" size={18} color="#fff" />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => openLink(`https://${item.website}`)}
          style={styles.actionContainer}>
          <Fontisto name="world-o" size={18} color="#fff" />
        </TouchableOpacity>
      </View>
      <View style={styles.addressContainer}>
        <View style={styles.mapContainer}>
          <MapView
            ref={map}
            provider={PROVIDER_GOOGLE}
            style={{
              ...StyleSheet.absoluteFillObject,
            }}
            region={{
              latitude: parseFloat(item.address.geo.lat),
              longitude: parseFloat(item.address.geo.lng),
              latitudeDelta: getLatitudeDelta,
              longitudeDelta: getLongitudeDelta,
            }}>
            <Marker
              coordinate={{
                latitude: parseFloat(item.address.geo.lat),
                longitude: parseFloat(item.address.geo.lng),
                latitudeDelta: getLatitudeDelta,
                longitudeDelta: getLongitudeDelta,
              }}>
              <Pointer background="#333" />
            </Marker>
          </MapView>
        </View>
        <View style={styles.addressTextContainer}>
          <ETASimpleText
            size={12}
            weight={Platform.OS === 'ios' ? '400' : '300'}
            color="#333"
            align="center">
            {item.address.street}, {item.address.suite}, {item.address.city},{' '}
            {item.address.zipcode}.
          </ETASimpleText>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 40,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderColor: '#2d2c3c',
    borderTopWidth: 0.4,
  },
  headerContainer: {
    flex: 0.2,
    alignItems: 'flex-start',
    minWidth: '100%',
    width: logoSize,
    height: logoSize,
    borderRadius: avatarRadius,
    marginTop: 10,
    marginLeft: 50,
    zIndex: 1000,
    backgroundColor: 'transparent',
  },
  headerText: {
    fontSize: 14,
    fontFamily: 'Arial',
    color: '#333',
  },
  usernameContainer: {
    height: 30,
    minWidth: 80,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5,
    zIndex: 999,
    backgroundColor: '#2D2C3C',
  },
  avatar: {
    height: logoSize,
    width: logoSize,
    marginBottom: 5,
  },
  actionsContainer: {
    height: 60,
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  actionContainer: {
    height: 40,
    width: 40,
    borderRadius: 20,
    marginHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#65D9E4',
  },
  dataContainer: {
    minHeight: 40,
    paddingVertical: 10,
    width: '85%',
    justifyContent: 'center',
    borderRadius: 7,
    borderWidth: 0.2,
    zIndex: 10,
    borderColor: '#333',
    marginHorizontal: 100,
    marginTop: 50,
    backgroundColor: '#fff',
  },
  addressContainer: {
    height: 190,
    width: '95%',
    padding: 7,
    marginHorizontal: 50,
    borderRadius: 10,
    borderWidth: 0.2,
    borderColor: '#333',
  },
  mapContainer: {
    flex: 1,
    width: '100%',
    borderRadius: 10,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  addressTextContainer: {
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
});

export default UserComponent;
