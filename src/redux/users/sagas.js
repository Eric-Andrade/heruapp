import {takeEvery, put, call} from 'redux-saga/effects';
import {GET_DATA_REQUEST, GET_DATA_REQUEST_SUCCESS} from './actions';
import {queryAPI} from '../query-api';

function* handler() {
  yield takeEvery(GET_DATA_REQUEST, getDataRequest);
}

function* getDataRequest(action) {
  try {
    const users = yield call(queryAPI, {
      endpoint: '',
      method: 'GET',
    });

    // console.log('[Sagas] users: ', {data: users});
    yield put({
      type: GET_DATA_REQUEST_SUCCESS,
      payload: {
        data: users,
      },
    });
  } catch (error) {
    console.log('[Users Saga] getDataRequest error: ', error);
  }
}

export {handler};
