import React from 'react';
import { Platform } from 'react-native';
import styled from 'styled-components/native';
import MainComponent from '@components/main';
import {
  PERMISSIONS,
  request,
  requestNotifications,
} from 'react-native-permissions';

const Root = styled.View`
  flex: 1;
  background-color: #fff;
`;

const MainScreen = () => {
  request(
		Platform.select({
		  android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
		  ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
		}),
	);
	
	requestNotifications(['alert', 'sound']).then(({status, settings}) => {
		// console.log('status notifications: ', {status, settings});
		// …
	});

  return (
    <Root>
      <MainComponent />
    </Root>
  );
};

export default React.memo(MainScreen)
