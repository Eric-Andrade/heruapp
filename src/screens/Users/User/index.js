import React, {useLayoutEffect} from 'react';
import styled from 'styled-components/native';
import UserComponent from '@components/users/user';

const Root = styled.View`
  flex: 1;
  background-color: #fff;
`;

const UserScreen = ({navigation, route}) => {
  const {item} = route?.params.params;

  useLayoutEffect(() => {
    navigation.setOptions({headerTitle: item.username});
  }, [navigation, route]);

  return (
    <Root>
      <UserComponent />
    </Root>
  );
};

export default React.memo(UserScreen);
