import React from 'react';
import styled from 'styled-components/native';
import UsersComponent from '@components/users';

const Root = styled.View`
  flex: 1;
  background-color: #fff;
`;

const UsersScreen = () => {
  return (
    <Root>
      <UsersComponent />
    </Root>
  );
};

export default React.memo(UsersScreen);
